#Autor: Miroslav Harag (xharag02)
#Datum: 4.4.2021
#Popis: translator
#Verzia: 1 

def eval(x):
    '''
    Funkcia prijma na vstupe retazet v tvare "operand binarna_operacia operand", "unarna_operacia operand" alebo "operand"
    Funkcia predpoklada ze v retazci je max. jedna operacia.
    Binarna operacia moze byt jedna z nasledujucich "+", "-","*" "/", "%", "^", 
    Unarna operacia moze byt jedna z nasledujucich "sin", "cos", "tan", "factorial"
    V pripade ak nie je zadana ziadna operacia funkcia iba prevedie operand na cislo
    '''
    # 1. zisti o aku operaciu sa jedna
    # 2. zisti v akej sustave su zadane operandy (pomocou funkcie findBase)
    # 3. pokusi sa prelozit operandy (pomocou funckie strToNum alebo findConst)
    # 4. zavola prislusnu operaciu z matlib
    # 5. vysledok vrati ako cislo
    return

def findBase(x):
    '''
    Funkcia zisti v akej sustave je zadane x.
    Ak nerozpozna ziadnu sustavu skonci s chybou.
    V pripade uspechu vrati dvojicu ("retazec bez predpon a pripon",ZAKLAD_SUSTAVY)
    napr. pre "0xABC" funckia vrati: ("ABC",16), pre "\"pi\"" vrati ("pi",0)
    Funkcia pracuje len s predponami a priponami. Neriesi spravnost x.
    '''
    #"..." -> ZAKLAD_SUSTAVY=0 - konstanta
    #0x... -> ZAKLAD_SUSTAVY=16
    #...b  -> ZAKLAD_SUSTAVY=2
    #      -> defaultne ZAKLAD_SUSTAVY=10
    return

#Autor: Miroslav Harag (xharag02)
#Datum: 4.4.2021
#Popis: Rozhranie mat. kniznice
#Verzia: 1

# Vsetky funkcie musia kontrolovat svoje svoje vstupy a v pripade chyby vratit exception (raise Exception("...")) 
# s vhodnou hlaskou


# jednoduche matematicke operacie

def add(x,y):
    '''
    Funkcia berie parametre x,y a vracia x+y
    '''
    return
def sub(x,y)
    '''
    Funkcia berie parametre x,y a vracia x-y
    '''
    return
def mul(x,y):
    '''
    Funkcia berie parametre x,y a vracia x*y
    '''
    return
def div(x,y)
    '''
    Funkcia berie parametre x,y a vracia x/y 
    alebo konci s chybou ak y=0
    '''
    return
def mod(x,y):
    '''
    Funkcia berie parametre x,y a vracia x mod y (zvysok po deleni)
    alebo konci s chybou ak y=0
    '''
    return
def pow(x,y):
    '''
    Funkcia berie parametre x,y a vracia x^y
    y moze byt lubovolne realne cislo
    '''
    return


# pokrocile matematicke operacie

def sin(x)
    '''
    Funkcia berie parametre x,y a vracia sin x
    '''
    return
def cos(x)
    '''
    Funkcia berie parametre x,y a vracia cos x
    '''
    return
def tan(x)
    '''
    Funkcia berie parametre x,y a vracia tan x
    '''
    return
def factorial(x)
    '''
    Funkcia berie parametre x,y a vracia faktorial z cisla x
    '''
    return


# Konverzia argumentov

def strToNum(x,base):
    '''
    Funkcia prevedie retazec "x" reprezentujuci cislo v sustave so zakladom base s presnostou.
    V pripade neplatneho retazca raise Exception("...").
    y moze byt lubovolne cele kladne cislo mensie ako 36
    '''
    return
def numToStr(x,base,precision):
    '''
    Funkcia prevedie cislo x do sustavy so zakladom base s presnostou na precision desatinnych miest.
    Funkcia nepridava ziadne prefixy ani sufixy (napr. 0x, b)
    y moze byt lubovolne cele kladne cislo mensie ako 36
    '''
    return
def findConst(x):
    '''
    Funkcia vrati hodnotu konstanty s nazvom x
    '''
    # zakladne konstanty "pi","e"
    return

import math
import pytest
from mathlib import *

## @file test_pokrocile_operacie.py
#  @brief Testy pre pokročilé operácie z matematickej knižnice
#  @author Rastislav Mozola xmozol02

"""Testovacie vstupy pre funkcie sin(x), cos(x), tan(x)"""
uhly = [0, 30, 45, 60, 90, 120, 135, 150, 180, 210, 225, 240, 270, 300, 315, 330, 360]


@pytest.mark.skip
def test_gonio(uhly, funkcia, ref):
    """
        Funkcia pre otestovanie goniometrickych funkcií
        @param uhly Pole so vstupnými uhlami pre goniometricke funkcie
        @param funkcia Testovaná goniometrická funkcia
        @param ref Referenčný výstup pre funkciu k daným uhlom
    """

    for i in range(len(uhly)):
        uhol = uhly[i]
        ref_uhol = ref[i]

        if not math.isnan(ref_uhol):
            """Ak je uhol definovaný"""
            assert funkcia(uhol) == ref_uhol
        else:
            """Inak očakávaj výnimku"""
            with pytest.raises(Exception):
                funkcia(uhol)


def test_sin():
    """
        Otestovanie funkcie sin(x)
    """

    """Referenčný výstup pre funkciu sin(x)"""
    sin_ref = [0, 0.5, math.sqrt(2) / 2, math.sqrt(3) / 2, 1, math.sqrt(3) / 2, math.sqrt(2) / 2, 0.5, 0,
               -0.5, -math.sqrt(2) / 2, -math.sqrt(3) / 2, -1, -math.sqrt(3) / 2, -math.sqrt(2) / 2, -0.5, 0]

    """Spustenie testov tabulkových hodnôt"""
    test_gonio(uhly, sin, sin_ref)

    """Test uhla s netabulkovou hodnotou"""
    assert "%.9f" % sin(43) == "%.9f" % 0.6819983600624985


def test_cos():
    """
        Otestovanie funkcie cos(x)
    """

    """Referenčný výstup pre funkciu cos(x)"""
    cos_ref = [1, math.sqrt(3) / 2, math.sqrt(2) / 2, 0.5, 0, -0.5, -math.sqrt(2) / 2, -math.sqrt(3) / 2,
               -1, -math.sqrt(3) / 2, -math.sqrt(2) / 2, -0.5, 0, 0.5, math.sqrt(2) / 2, math.sqrt(3) / 2, 1]

    """Spustenie testov tabulkových hodnôt"""
    test_gonio(uhly, cos, cos_ref)

    """Test uhla s netabulkovou hodnotou"""
    assert "%.9f" % cos(43) == "%.9f" % 0.7313537016191705


def test_tan():
    """
        Otestovanie funkcie tan(x)
    """

    """Referenčný výstup pre funkciu tan(x)"""
    tan_ref = [0, 1 / math.sqrt(3), 1, math.sqrt(3), float("nan"), -math.sqrt(3), -1, -1 / math.sqrt(3), 0,
               1 / math.sqrt(3), 1, math.sqrt(3), float("nan"), -math.sqrt(3), -1, -1 / math.sqrt(3), 0]

    """Spustenie testov tabulkových hodnôt"""
    test_gonio(uhly, tan, tan_ref)

    """Test uhla s netabulkovou hodnotou"""
    assert "%.9f" % tan(43) == "%.9f" % 0.9325150861376618


def test_factorial():
    """
        Otestovanie funkcie factorial(x)
    """

    """Testy pre hodnotu 0"""
    assert factorial(0) == 1
    assert factorial(0.0) == 1

    """Testy pre celé čísla"""
    assert factorial(1) == 1
    assert factorial(10) == 3628800
    with pytest.raises(Exception):
        factorial(-1)
    with pytest.raises(Exception):
        factorial(-10)

    """Testy pre desatinné čísla"""
    with pytest.raises(Exception):
        factorial(1.5)
    with pytest.raises(Exception):
        factorial(-1.5)

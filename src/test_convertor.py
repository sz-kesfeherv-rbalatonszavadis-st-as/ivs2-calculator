## @file test_convertor.py
#  @brief Testy pre matematické operácie, a konverziu argumentov
#  @author Rastislav Mozola xmozol02

import pytest
from convertor import *


@pytest.mark.skip
##Testovanie výnimiek funkcie strToNum(x, base) v rámci rovnakej, alebo rôznej sústavy
#@param same_base - (true) Testovanie výnimiek v rámci rovnakej(desiatkovej) sústavy
#                - (false) Testovanie výnimiek mimo rovnakej(desiatkovej) sústavy
def test_strToNum_Exceptions(same_base: bool):

    """Na základe same base sa pouzije daná sústava"""
    if same_base:
        base = 10
    else:
        base = 3

    """Testy konverzie pre znaky, očakávajúce chybu"""
    with pytest.raises(Exception):
        strToNum("ABC", base)
    with pytest.raises(Exception):
        strToNum("#", base)
    with pytest.raises(Exception):
        strToNum("", base)
    with pytest.raises(Exception):
        strToNum(" ", base)
    with pytest.raises(Exception):
        strToNum("\n", base)
    with pytest.raises(Exception):
        strToNum(".", base)
    with pytest.raises(Exception):
        strToNum(" . ", base)
    with pytest.raises(Exception):
        strToNum("A.C", base)
    with pytest.raises(Exception):
        strToNum("9.C", base)
    with pytest.raises(Exception):
        strToNum("C.9", base)

    """Testy pre neplatne číslo, očakavajúce chybu"""
    with pytest.raises(Exception):
        strToNum("1.", base)
    with pytest.raises(Exception):
        strToNum(".2", base)
    with pytest.raises(Exception):
        strToNum(" .2 ", base)

    """Testy pre číslo mimo danej sústavy"""
    with pytest.raises(Exception):
        strToNum("1023A", base)
    with pytest.raises(Exception):
        strToNum("01023B", base)


@pytest.mark.skip
##Testovanie výnimiek funkcie numToStr(x, base) v rámci rovnakej, alebo rôznej sústavy
#@param same_base - (true) Testovanie výnimiek v rámci rovnakej(desiatkovej) sústavy
#                 - (false) Testovanie výnimiek mimo rovnakej(desiatkovej) sústavy
def test_numToStr_Exceptions(same_base: bool):

    precision = 1

    """Na základe same base sa pouzije daná sústava"""
    if same_base:
        base = 10
    else:
        base = 3

    """Testy pre neplatný precision"""
    with pytest.raises(Exception):
        numToStr(10, base, 0.0)
    with pytest.raises(Exception):
        numToStr(10, base, 0.1)
    with pytest.raises(Exception):
        numToStr(10, base, 1.1)

    """Test pre vysoký precission"""
    with pytest.raises(Exception) as exception_raised:
        # pozor na Zero Division Error
        numToStr(10, base, 1000000000000000000000000000000000000000000)

    """Test správnosti výnimky pre vysoký precision"""
    exception_raised = exception_raised.value
    if exception_raised.args[0] == "float division by zero":
        assert 1 == 0


def test_strToNum():
    """
    Otestovanie funkcie strToNum(x, base) v rámci rovnakej sústavy
    """

    """Testy konverzie pre celé čísla"""
    assert strToNum("10", 10) == 10
    assert strToNum("-010", 10) == -10
    assert strToNum("+010", 10) == 10
    assert strToNum("-0", 10) == 0
    assert strToNum("+00", 10) == 0

    """Testy konverzie pre desatinné čísla"""
    assert strToNum("0.5", 10) == 0.5
    assert strToNum("-0.5", 10) == -0.5
    assert strToNum("+0.5", 10) == 0.5
    assert strToNum("-0.0", 10) == 0
    assert strToNum("+0.0", 10) == 0
    assert strToNum("+0.01", 10) == 0.01
    assert strToNum("-0.99", 10) == -0.99

    test_strToNum_Exceptions(True)


def test_base_strToNum():
    """
    Otestovanie funkcie strToNum(x, base) mimo rovnakej sústavy
    """

    """Testy konverzie pre celé čísla"""
    assert strToNum("001010", 2) == 10
    assert strToNum("-00202", 3) == -20
    assert strToNum("+00103", 4) == 19
    assert strToNum("-0", 2) == 0
    assert strToNum("+000000", 2) == 0

    """Testy konverzie pre desatinné čísla"""
    assert strToNum("0.100", 2) == 0.5
    assert strToNum("-0.01", 2) == -0.25
    assert strToNum("+176.56", 8) == 126.71875
    assert strToNum("-Z54.J5", 36) == -45544.53163580246913580247
    assert strToNum("+0.00", 16) == 0
    assert strToNum("+000AK45.500", 23) == 132347.21739130434782608696
    assert strToNum("-000000.9900", 11) == -0.89256198347107438017

    """Testy pre neplatný base"""
    with pytest.raises(Exception):
        strToNum("10", 0)
    with pytest.raises(Exception):
        strToNum("10", 1)
    with pytest.raises(Exception):
        strToNum("10", 2.1)
    with pytest.raises(Exception):
        strToNum("10", 2.)
    with pytest.raises(Exception):
        strToNum("10", 0.1)
    with pytest.raises(Exception):
        strToNum("10", -1)
    with pytest.raises(Exception):
        strToNum("10", 37)

    test_strToNum_Exceptions(False)


def test_numToStr():
    """
    Otestovanie funkcie numToStr(x, base, precision) v rámci rovnakej sústavy
    """

    """Testy konverzie zo string na int pre celé čísla"""
    assert numToStr(10, 10, 0) == "10"
    assert numToStr(-10, 10, 0) == "-10"
    assert numToStr(+10, 10, 0) == "10"
    assert numToStr(-0, 10, 0) == "0"
    assert numToStr(+00, 10, 0) == "0"

    """Testy konverzie zo string na int pre desatinné čísla"""
    assert numToStr(0.5, 10, 1) == "0.5"
    assert numToStr(-0.5, 10, 1) == "-0.5"
    assert numToStr(+0.5, 10, 1) == "0.5"
    assert numToStr(-0.0, 10, 0) == "0"
    assert numToStr(+00.00, 10, 0) == "0"
    assert numToStr(+0.01, 10, 2) == "0.01"
    assert numToStr(-00.99, 10, 2) == "-0.99"

    """Testy pre precision -1 s kladnými číslami"""
    assert numToStr(0.500, 10, -1) == "0.5"
    assert numToStr(0.00000, 10, -1) == "0"
    assert numToStr(10.0, 10, -1) == "10"
    assert numToStr(1.01, 10, -1) == "1.01"
    assert numToStr(7.7, 10, -1) == "7.7"

    """Testy pre precision -1 so zápornými číslami"""
    assert numToStr(-0.500, 10, -1) == "-0.5"
    assert numToStr(-0.00000, 10, -1) == "0"
    assert numToStr(-10.0, 10, -1) == "-10"
    assert numToStr(-1.01, 10, -1) == "-1.01"
    assert numToStr(-7.7, 10, -1) == "-7.7"

    test_numToStr_Exceptions(True)


def test_base_numToStr():
    """
    Otestovanie funkcie numToStr(x, base) mimo rovnakej sústavy
    """

    """Testy konverzie pre celé čísla"""
    assert numToStr(10, 2, 0) == "1010"
    assert numToStr(-20, 3, 1) == "-202.0"
    assert numToStr(+19, 4, 2) == "103.00"
    assert numToStr(-0, 2, 0) == "0"
    assert numToStr(-0, 2, 1) == "0.0"
    assert numToStr(-0, 2, 2) == "0.00"
    assert numToStr(+0, 2, 3) == "0.000"

    """Testy konverzie pre desatinné čísla"""
    assert numToStr(0.5, 2, 1) == "0.1"
    assert numToStr(-0.25, 2, 2) == "-0.01"
    assert numToStr(+126.71875, 8, 1) == "176.6"
    assert numToStr(-45544.53163580246913580247, 36, 5) == "-Z54.J5000"
    assert numToStr(+00.00, 16, 2) == "0.00"
    assert numToStr(+132347.21739130434782608696, 23, 0) == "AK45"
    assert numToStr(-0.89256198347107438017, 11, 2) == "-0.99"

    """Testy pre precision -1 s kladnými číslami"""
    assert numToStr(0.5, 2, -1) == "0.1"
    assert numToStr(+126.71875, 8, -1) == "176.56"
    assert numToStr(+00.00, 16, -1) == "0"
    assert numToStr(+132347.21739130434782608696, 23, -1) == "AK45.4MMMMMMLGK"

    """Testy pre precision -1 so zápornými číslami"""
    assert numToStr(-0.25, 2, -1) == "-0.01"
    assert numToStr(-45544.53163580246913580247, 36, -1) == "-Z54.J5"
    assert numToStr(-0.89256198347107438017, 11, -1) == "-0.99"

    """Testy pre neplatný base"""
    with pytest.raises(Exception):
        numToStr(1, 0, 1)
    with pytest.raises(Exception):
        numToStr(1, 1, 1)
    with pytest.raises(Exception):
        numToStr(1, 2.1, 1)
    with pytest.raises(Exception):
        numToStr(1, 2., 1)
    with pytest.raises(Exception):
        numToStr(1, 0.1, 1)
    with pytest.raises(Exception):
        numToStr(1, .1, 1)
    with pytest.raises(Exception):
        numToStr(1, -1, 1)
    with pytest.raises(Exception):
        numToStr(1, 37, 1)

    test_numToStr_Exceptions(False)


def test_findConst():
    """
    Otestovanie funkcie findConst(x)
    """

    """Testy pre definované konštanty"""
    assert findConst("pi") == 3.14159265359
    assert findConst("e") == 2.71828182846

    """Testy pre nedefinované konštanty"""
    with pytest.raises(Exception):
        findConst("")
    with pytest.raises(Exception):
        findConst(".")

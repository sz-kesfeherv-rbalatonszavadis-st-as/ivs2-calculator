##@file convertor.py
# @brief Knižnica s matematickými operáciami, konverzia argumentov a prevody číselných sústav
# @author Miroslav Harag xharag02, Matúš Remeň xremen01

import string

intToChar = string.digits + string.ascii_uppercase
charToInt = {intToChar[i]:i for i in range(0,36)}

##Funkcia na konverziu stringu na cislo
# @param x string reprezentujuci cislo
# @param base zaklad sustavy v ktorej je zadane cislo x
# @return Zkonvertovane cislo
def strToNum(x,base):
    if 1 >= base or base > 36 or not isinstance(base, int):
        raise ValueError(str(base) + " Nie je podporovany zaklad sustavy")

    result = 0

    """osetrenie znamienka"""
    if x[0] == '-':
        sign = -1
        x = x[1:]
    elif x[0] == '+':
        x = x[1:]
        sign = 1
    else:
        sign = 1


    """rozdelenie na celu a desatinnu cast"""
    if "." not in x:
        intPart = x
        if len(intPart) == 0:
            raise ValueError("Retazec: " + x + " nie je cislo v sustave: " + str(base))


    else:
        intPart, floatPart = x.split(".")
        if len(intPart) == 0 or len(floatPart) == 0:
            raise ValueError("Retazec: " + x + " nie je cislo v sustave: " + str(base))


    """prevod celej casti"""
    for i in enumerate(reversed(intPart)):
        if (i[1] not in charToInt) or (charToInt[i[1]] >= base):
            raise ValueError("Retazec: " + x + " nie je cislo v sustave: " + str(base))
        result += base**i[0] * charToInt[i[1]]

    """koniec ak cislo nema desatinnu cast"""
    if "." not in x:
        return result * sign

    """prevod desatinnej casti"""
    for i in enumerate(floatPart):
        if (i[1] not in charToInt) or (charToInt[i[1]] >= base):
            raise ValueError("Retazec: " + x + " nie je cislo v sustave: " + str(base))
        result += base**-(i[0] + 1) * charToInt[i[1]]

    return result * sign


##Funkcia prevedie cislo x na string v sustave so zakladom base.
# @param x Cislo ktore ma byt prevedena
# @param base Zaklad sustavy v ktorej ma byt vysledne cislo
# @param precision pocet desatinnych miest vo vysledku
# @return String reprezentujuci cislo x v sustave base
def numToStr(x,base,precision):
    """automaticke orezavanie vystupu"""
    auto_crop = False
    if precision == -1:
        precision = 10
        auto_crop = True

    """kontrola argumentov"""
    if 1 >= base or base > 36 or not isinstance(base, int):
        raise ValueError(str(base) + " Nie je podporovany zaklad sustavy.")
    if 0 > precision or precision > 100 or not isinstance(precision, int):
        raise ValueError("Precision musi byt kladne cele cislo.")
    
    """osetrenie znamienka"""
    sign = -1 if x < 0 else 1
    x *= sign

    """zabeznecenie pozadovanej presnosti"""
    x = int(round(x / base ** -precision))

    result = ''
    while x:
        result += intToChar[x % base]
        x //= base

    """osetrenie zaciatocnych nul""" 
    result += '0' * (precision + 1 - len(result))

    if sign == -1:
        result += '-'

    result = result[::-1]
    if precision != 0:
        result = result[:-precision] + '.' + result[-precision:]

    if auto_crop:
        return result.rstrip("0").rstrip(".")
    else: 
        return result



##Funkcia na ziskanie ciselnej hodnoty mat. konstanty 
# @param x Nazov konstanty 
# @return Ciselna hodnota konstanty x. 
def findConst(x):
    constants = {
    "pi": 3.14159265359,
    "e": 2.71828182846,
    }

    if x not in constants:
        raise ValueError("Neznama konstanta: " + x)
    return constants[x]


##Funkcia na prevádzanie čísel medzi sústavami
# @param num Číslo zo vstupu kalkulačky, ktoré sa bude prevádzať
# @param from_base Sústava, v ktorej je vstupné číslo
# @param to_base Sústava, do ktorej užívatel požaduje previesť vstupné číslo num
# @return Číslo zo vstupu prevedené do požadovanej sústavy
def convert(num: str, from_base: int, to_base: int) -> str:
    result = strToNum(num, from_base)
    return numToStr(result, to_base, 0)

